/***************************************
接线说明:
//-------------------------------------------------------------------------------------
#define LCD_CTRL   	  	GPIOB		//定义TFT数据端口
#define LCD_LED        	GPIO_Pin_9  //PB9 连接至TFT -LED
#define LCD_RS         	GPIO_Pin_10	//PB10连接至TFT --RS
#define LCD_CS        	GPIO_Pin_11 //PB11 连接至TFT --CS
#define LCD_RST     	GPIO_Pin_12	//PB12连接至TFT --RST
#define LCD_SCL        	GPIO_Pin_13	//PB13连接至TFT -- CLK
#define LCD_SDA        	GPIO_Pin_15	//PB15连接至TFT - SDI
//VCC:可以接5V也可以接3.3V
//LED:可以接5V也可以接3.3V或者使用任意空闲IO控制(高电平使能)
//GND：接电源地
//说明：如需要尽可能少占用IO，可以将LCD_CS接地，LCD_LED接3.3V，LCD_RST接至单片机复位端，
//将可以释放3个可用IO
//-----------------------------------------------------------------------------------------*/
#include "stm32f10x.h"
#include "box.h"
#include "Lcd_Driver.h"
#include "GUI.h"
#include "box.h"
#include "time.h"
#include "SysTick.h"
#include "key.h"
#include "stdio.h"
#include "stdlib.h"
#include "adc.h"

u8 what,speed=80,i=0,game=1,leave=1,suijishu[5];
u16 x=60,y=0,fengshu=0;
u8 shuaxing=0;
u8 zhuangtai[23][16]=
{
  {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},//**********0**********//
	{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},//**********1**********//
	{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},//**********2**********//
	{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},//**********3**********//
	{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},//**********4**********//
	{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},//**********5**********//
	{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},//**********6**********//
	{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},//**********7**********//
	{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},//**********8**********//
	{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},//**********9**********//
	{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},//**********10**********//
	{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},//**********11**********//
	{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},//**********12**********//
	{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},//**********13**********//
	{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},//**********14**********//
	{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},//**********15**********//
	{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},//**********16**********//
	{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},//**********17**********//
	{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},//**********18**********//
	{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},//**********19**********//
	{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},//**********20**********//
	{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},//**********21**********//
	{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},//**********22**********//
};












/*************************************************
功能：更新一个状态到数组
*************************************************/
void Draw_a_zhuangtai(u16 x,u16 y)
{
	  zhuangtai[y/10][x/10+1]=1;
}


/*************************************************
功能：删除一个状态到数组
*************************************************/
void Del_a_zhuangtai(u16 x,u16 y)
{
	  zhuangtai[y/10][x/10+1]=0;
}



/*************************************************
功能：画对应的状态图形
*************************************************/
void Draw_zhuangtai_tuxing(u16 x,u16 y,u8 what)
{
	switch (what)
	{
		case 1:
		{
		Draw_a_zhuangtai(x,y);
		Draw_a_zhuangtai(x+10,y);
		Draw_a_zhuangtai(x,y+10);
		Draw_a_zhuangtai(x+10,y+10);
		}
		break;
		
		case 2:
		{
		Draw_a_zhuangtai(x,y);
		Draw_a_zhuangtai(x+10,y);
		Draw_a_zhuangtai(x+20,y);
		Draw_a_zhuangtai(x+30,y);
		}
		break;
		
		case 3:
		{
		Draw_a_zhuangtai(x,y);
		Draw_a_zhuangtai(x,y+10);
		Draw_a_zhuangtai(x,y+20);
		Draw_a_zhuangtai(x,y+30);
		}
		break;
		
		case 4:
		{
		Draw_a_zhuangtai(x+10,y);
		Draw_a_zhuangtai(x,y+10);
		Draw_a_zhuangtai(x+10,y+10);
		Draw_a_zhuangtai(x+20,y+10);
		}
		break;
		
		case 5:
		{
		Draw_a_zhuangtai(x+10,y+10);
		Draw_a_zhuangtai(x,y);
		Draw_a_zhuangtai(x,y+10);
		Draw_a_zhuangtai(x,y+20);
		}
		break;

		case 6:
		{
		Draw_a_zhuangtai(x,y+10);
		Draw_a_zhuangtai(x+10,y);
		Draw_a_zhuangtai(x+10,y+10);
		Draw_a_zhuangtai(x+10,y+20);
		}
		break;
		
		case 7:
		{
		Draw_a_zhuangtai(x+10,y+10);
		Draw_a_zhuangtai(x,y);
		Draw_a_zhuangtai(x+10,y);
		Draw_a_zhuangtai(x+20,y);
		}
		break;
	
		case 8:
		{
		Draw_a_zhuangtai(x,y);
		Draw_a_zhuangtai(x,y+10);
		Draw_a_zhuangtai(x,y+20);
		Draw_a_zhuangtai(x+10,y+20);
		}
		break;
		
		case 9:
		{
		Draw_a_zhuangtai(x,y);
		Draw_a_zhuangtai(x,y+10);
		Draw_a_zhuangtai(x+10,y);
		Draw_a_zhuangtai(x+20,y);
		}
		break;
		
		case 10:
		{
		Draw_a_zhuangtai(x,y);
		Draw_a_zhuangtai(x+10,y);
		Draw_a_zhuangtai(x+10,y+10);
		Draw_a_zhuangtai(x+10,y+20);
		}
		break;
		
		case 11:
		{
		Draw_a_zhuangtai(x,y+10);
		Draw_a_zhuangtai(x+10,y+10);
		Draw_a_zhuangtai(x+20,y+10);
		Draw_a_zhuangtai(x+20,y);
		}
		break;
		
		case 12:
		{
		Draw_a_zhuangtai(x+10,y);
		Draw_a_zhuangtai(x+10,y+10);
		Draw_a_zhuangtai(x+10,y+20);
		Draw_a_zhuangtai(x,y+20);
		}
		break;
		
		case 13:
		{
		Draw_a_zhuangtai(x,y);
		Draw_a_zhuangtai(x+10,y);
		Draw_a_zhuangtai(x+20,y);
		Draw_a_zhuangtai(x+20,y+10);
		}
		break;
		
		case 14:
		{
		Draw_a_zhuangtai(x,y);
		Draw_a_zhuangtai(x+10,y);
		Draw_a_zhuangtai(x,y+10);
		Draw_a_zhuangtai(x,y+20);
		}
		break;
		
		case 15:
		{
		Draw_a_zhuangtai(x,y);
		Draw_a_zhuangtai(x,y+10);
		Draw_a_zhuangtai(x+10,y+10);
		Draw_a_zhuangtai(x+20,y+10);
		}
		break;
		
		
		case 16:
		{
		Draw_a_zhuangtai(x+10,y);
		Draw_a_zhuangtai(x,y+10);
		Draw_a_zhuangtai(x+10,y+10);
		Draw_a_zhuangtai(x,y+20);
		}
		break;
		
		case 17:
		{
		Draw_a_zhuangtai(x,y);
		Draw_a_zhuangtai(x+10,y);
		Draw_a_zhuangtai(x+10,y+10);
		Draw_a_zhuangtai(x+20,y+10);
		}
		break;
		
		case 18:
		{
		Draw_a_zhuangtai(x,y);
		Draw_a_zhuangtai(x,y+10);
		Draw_a_zhuangtai(x+10,y+10);
		Draw_a_zhuangtai(x+10,y+20);
		}
		break;
		
		case 19:
		{
		Draw_a_zhuangtai(x,y+10);
		Draw_a_zhuangtai(x+10,y+10);
		Draw_a_zhuangtai(x+10,y);
		Draw_a_zhuangtai(x+20,y);
		}
		break;
	
	
	
	
	}

}



/*************************************************
功能：删除对应的状态图形
*************************************************/
void Del_zhuangtai_tuxing(u16 x,u16 y,u8 what)
{
	switch (what)
	{
		case 1:
		{
		Del_a_zhuangtai(x,y);
		Del_a_zhuangtai(x+10,y);
		Del_a_zhuangtai(x,y+10);
		Del_a_zhuangtai(x+10,y+10);
		}
		break;
		
		case 2:
		{
		Del_a_zhuangtai(x,y);
		Del_a_zhuangtai(x+10,y);
		Del_a_zhuangtai(x+20,y);
		Del_a_zhuangtai(x+30,y);
		}
		break;
		
		case 3:
		{
		Del_a_zhuangtai(x,y);
		Del_a_zhuangtai(x,y+10);
		Del_a_zhuangtai(x,y+20);
		Del_a_zhuangtai(x,y+30);
		}
		break;
		
		case 4:
		{
		Del_a_zhuangtai(x+10,y);
		Del_a_zhuangtai(x,y+10);
		Del_a_zhuangtai(x+10,y+10);
		Del_a_zhuangtai(x+20,y+10);
		}
		break;
		
		case 5:
		{
		Del_a_zhuangtai(x+10,y+10);
		Del_a_zhuangtai(x,y);
		Del_a_zhuangtai(x,y+10);
		Del_a_zhuangtai(x,y+20);
		}
		break;

		case 6:
		{
		Del_a_zhuangtai(x,y+10);
		Del_a_zhuangtai(x+10,y);
		Del_a_zhuangtai(x+10,y+10);
		Del_a_zhuangtai(x+10,y+20);
		}
		break;
		
		case 7:
		{
		Del_a_zhuangtai(x+10,y+10);
		Del_a_zhuangtai(x,y);
		Del_a_zhuangtai(x+10,y);
		Del_a_zhuangtai(x+20,y);
		}
		break;
	
		case 8:
		{
		Del_a_zhuangtai(x,y);
		Del_a_zhuangtai(x,y+10);
		Del_a_zhuangtai(x,y+20);
		Del_a_zhuangtai(x+10,y+20);
		}
		break;
		
		case 9:
		{
		Del_a_zhuangtai(x,y);
		Del_a_zhuangtai(x,y+10);
		Del_a_zhuangtai(x+10,y);
		Del_a_zhuangtai(x+20,y);
		}
		break;
		
		case 10:
		{
		Del_a_zhuangtai(x,y);
		Del_a_zhuangtai(x+10,y);
		Del_a_zhuangtai(x+10,y+10);
		Del_a_zhuangtai(x+10,y+20);
		}
		break;
		
		case 11:
		{
		Del_a_zhuangtai(x,y+10);
		Del_a_zhuangtai(x+10,y+10);
		Del_a_zhuangtai(x+20,y+10);
		Del_a_zhuangtai(x+20,y);
		}
		break;
		
		case 12:
		{
		Del_a_zhuangtai(x+10,y);
		Del_a_zhuangtai(x+10,y+10);
		Del_a_zhuangtai(x+10,y+20);
		Del_a_zhuangtai(x,y+20);
		}
		break;
		
		case 13:
		{
		Del_a_zhuangtai(x,y);
		Del_a_zhuangtai(x+10,y);
		Del_a_zhuangtai(x+20,y);
		Del_a_zhuangtai(x+20,y+10);
		}
		break;
		
		case 14:
		{
		Del_a_zhuangtai(x,y);
		Del_a_zhuangtai(x+10,y);
		Del_a_zhuangtai(x,y+10);
		Del_a_zhuangtai(x,y+20);
		}
		break;
		
		case 15:
		{
		Del_a_zhuangtai(x,y);
		Del_a_zhuangtai(x,y+10);
		Del_a_zhuangtai(x+10,y+10);
		Del_a_zhuangtai(x+20,y+10);
		}
		break;
		
		case 16:
		{
		Del_a_zhuangtai(x+10,y);
		Del_a_zhuangtai(x,y+10);
		Del_a_zhuangtai(x+10,y+10);
		Del_a_zhuangtai(x,y+20);
		}
		break;
		
		case 17:
		{
		Del_a_zhuangtai(x,y);
		Del_a_zhuangtai(x+10,y);
		Del_a_zhuangtai(x+10,y+10);
		Del_a_zhuangtai(x+20,y+10);
		}
		break;
		
		case 18:
		{
		Del_a_zhuangtai(x,y);
		Del_a_zhuangtai(x,y+10);
		Del_a_zhuangtai(x+10,y+10);
		Del_a_zhuangtai(x+10,y+20);
		}
		break;
		
		case 19:
		{
		Del_a_zhuangtai(x,y+10);
		Del_a_zhuangtai(x+10,y+10);
		Del_a_zhuangtai(x+10,y);
		Del_a_zhuangtai(x+20,y);
		}
		break;
	}

}

/*************************************************
功能：向下移动状态
*************************************************/

void Down_zhuangtai_move(u16 x,u16 y,u16 what)
{
	Del_zhuangtai_tuxing(x,y,what);
	Draw_zhuangtai_tuxing(x,y+10,what);
}

/*************************************************
功能：向左移动状态
*************************************************/
void Left_zhuangtai_move(u16 x,u16 y,u8 what)
{
	Del_zhuangtai_tuxing(x,y,what);
	Draw_zhuangtai_tuxing(x-10,y,what);
}

/*************************************************
功能：向右移动状态
*************************************************/
void Right_zhuangtai_move(u16 x,u16 y,u8 what)
{
	Del_zhuangtai_tuxing(x,y,what);
	Draw_zhuangtai_tuxing(x+10,y,what);
}









void Down(u16 x,u16 y,u8 what)//调用方向移动函数
{
	Down_zhuangtai_move(x,y,what);
	Down_tuxing_move(x,y,what);			
}


void Left(u16 x,u16 y,u8 what)
{
	Left_zhuangtai_move(x,y,what);
	Left_tuxing_move(x,y,what);			
}


void Right(u16 x,u16 y,u8 what)
{
	Right_zhuangtai_move(x,y,what);
	Right_tuxing_move(x,y,what);			
}


void Del(u16 x,u16 y,u8 what)
{
	Del_tuxing(x,y,what);
	Del_zhuangtai_tuxing(x,y,what);			
}



void Draw(u16 x,u16 y,u8 what)
{
	Draw_tuxing(x,y,what);
	Draw_zhuangtai_tuxing(x,y,what);			
}




void change()
{
	switch(what)
	{
		case 1:break;
		
		case 2:Del(x,y,2);Draw(x,y,3);what=3;break;
		case 3:Del(x,y,3);Draw(x,y,2);what=2;break;
		
		case 4:Del(x,y,4);Draw(x,y,5);what=5;break;
		case 5:Del(x,y,5);Draw(x,y,7);what=7;break;
		case 6:Del(x,y,6);Draw(x,y,4);what=4;break;
		case 7:Del(x,y,7);Draw(x,y,6);what=6;break;
		
		case 8:Del(x,y,8);Draw(x,y,9);what=9;break;
		case 9:Del(x,y,9);Draw(x,y,10);what=10;break;
		case 10:Del(x,y,10);Draw(x,y,11);what=11;break;
		case 11:Del(x,y,11);Draw(x,y,8);what=8;break;
		
		case 12:Del(x,y,12);Draw(x,y,15);what=15;break;
		case 13:Del(x,y,13);Draw(x,y,12);what=12;break;
		case 14:Del(x,y,14);Draw(x,y,13);what=13;break;
		case 15:Del(x,y,15);Draw(x,y,14);what=14;break;
		
		case 16:Del(x,y,16);Draw(x,y,17);what=17;break;
		case 17:Del(x,y,17);Draw(x,y,16);what=16;break;
		
		case 18:Del(x,y,18);Draw(x,y,19);what=19;break;
		case 19:Del(x,y,19);Draw(x,y,18);what=18;break;
	
	}





}


void change_zhuangtai()
{
	switch(what)
	{
		case 1:break;
		case 2:Del_zhuangtai_tuxing(x,y,2);Draw_zhuangtai_tuxing(x,y,3);break;
		case 3:Del_zhuangtai_tuxing(x,y,3);Draw_zhuangtai_tuxing(x,y,2);break;
		case 4:Del_zhuangtai_tuxing(x,y,4);Draw_zhuangtai_tuxing(x,y,5);break;
		case 5:Del_zhuangtai_tuxing(x,y,5);Draw_zhuangtai_tuxing(x,y,7);break;
		case 6:Del_zhuangtai_tuxing(x,y,6);Draw_zhuangtai_tuxing(x,y,4);break;
		case 7:Del_zhuangtai_tuxing(x,y,7);Draw_zhuangtai_tuxing(x,y,6);break;
		case 8:Del_zhuangtai_tuxing(x,y,8);Draw_zhuangtai_tuxing(x,y,9);break;
		case 9:Del_zhuangtai_tuxing(x,y,9);Draw_zhuangtai_tuxing(x,y,10);break;
		case 10:Del_zhuangtai_tuxing(x,y,10);Draw_zhuangtai_tuxing(x,y,11);break;
		case 11:Del_zhuangtai_tuxing(x,y,11);Draw_zhuangtai_tuxing(x,y,8);break;
		case 12:Del_zhuangtai_tuxing(x,y,12);Draw_zhuangtai_tuxing(x,y,15);break;
		case 13:Del_zhuangtai_tuxing(x,y,13);Draw_zhuangtai_tuxing(x,y,12);break;
		case 14:Del_zhuangtai_tuxing(x,y,14);Draw_zhuangtai_tuxing(x,y,13);break;
		case 15:Del_zhuangtai_tuxing(x,y,15);Draw_zhuangtai_tuxing(x,y,14);break;
		case 16:Del_zhuangtai_tuxing(x,y,16);Draw_zhuangtai_tuxing(x,y,17);break;
		case 17:Del_zhuangtai_tuxing(x,y,17);Draw_zhuangtai_tuxing(x,y,16);break;
		case 18:Del_zhuangtai_tuxing(x,y,18);Draw_zhuangtai_tuxing(x,y,19);break;
		case 19:Del_zhuangtai_tuxing(x,y,19);Draw_zhuangtai_tuxing(x,y,18);break;
	
	}





}






void lie_move(u16 y)//交换上下两行的缓存值
{
	u8 i;
	y=y/10;
	for(i=1;i<15;i++)
	{
		if(zhuangtai[y][i]==1)
		{
			zhuangtai[y][i]=zhuangtai[y+1][i];
			zhuangtai[y+1][i]=1;
			
				Del_realbox((i-1)*10,y*10);
			Draw_realbox((i-1)*10,(y+1)*10);
		}
		else if(zhuangtai[y][i]==0)
		{
			zhuangtai[y][i]=zhuangtai[y+1][i];
			zhuangtai[y+1][i]=0;
			
				Del_realbox((i-1)*10,y*10);
			Del_realbox((i-1)*10,(y+1)*10);
		}
	}
}


void Del_lie(u16 y)//删除一行
{
	u8 i;
	y=y/10;
	for(i=1;i<15;i++)
	{
		zhuangtai[y][i]=0;
		Del_realbox((i-1)*10,y*10);
	}
}







void display_leave()
{
	switch(leave)
	{
		case 0:Gui_DrawFont_GBK16(155,182,RED,WHITE,"0");break;	
		case 1:Gui_DrawFont_GBK16(155,182,RED,WHITE,"1");break;	
		case 2:Gui_DrawFont_GBK16(155,182,RED,WHITE,"2");break;	
		case 3:Gui_DrawFont_GBK16(155,182,RED,WHITE,"3");break;	
		case 4:Gui_DrawFont_GBK16(155,182,RED,WHITE,"4");break;	
		case 5:Gui_DrawFont_GBK16(155,182,RED,WHITE,"5");break;	
		case 6:Gui_DrawFont_GBK16(155,182,RED,WHITE,"6");break;	
		case 7:Gui_DrawFont_GBK16(155,182,RED,WHITE,"7");break;
		case 8:Gui_DrawFont_GBK16(155,182,RED,WHITE,"8");break;	
		case 9:Gui_DrawFont_GBK16(155,182,RED,WHITE,"9");break;			
	}
}

void display_fengshu()
{
	u8 shi,ge;
	shi=fengshu/10;
	ge=fengshu%10;
	switch(shi)
	{
		case 0:Gui_DrawFont_GBK16(150,122,RED,WHITE,"0");break;	
		case 1:Gui_DrawFont_GBK16(150,122,RED,WHITE,"1");break;	
		case 2:Gui_DrawFont_GBK16(150,122,RED,WHITE,"2");break;	
		case 3:Gui_DrawFont_GBK16(150,122,RED,WHITE,"3");break;	
		case 4:Gui_DrawFont_GBK16(150,122,RED,WHITE,"4");break;	
		case 5:Gui_DrawFont_GBK16(150,122,RED,WHITE,"5");break;	
		case 6:Gui_DrawFont_GBK16(150,122,RED,WHITE,"6");break;	
		case 7:Gui_DrawFont_GBK16(150,122,RED,WHITE,"7");break;
		case 8:Gui_DrawFont_GBK16(150,122,RED,WHITE,"8");break;	
		case 9:Gui_DrawFont_GBK16(150,122,RED,WHITE,"9");break;			
	}
	
	switch(ge)
	{
		case 0:Gui_DrawFont_GBK16(160,122,RED,WHITE,"0");break;	
		case 1:Gui_DrawFont_GBK16(160,122,RED,WHITE,"1");break;	
		case 2:Gui_DrawFont_GBK16(160,122,RED,WHITE,"2");break;	
		case 3:Gui_DrawFont_GBK16(160,122,RED,WHITE,"3");break;	
		case 4:Gui_DrawFont_GBK16(160,122,RED,WHITE,"4");break;	
		case 5:Gui_DrawFont_GBK16(160,122,RED,WHITE,"5");break;	
		case 6:Gui_DrawFont_GBK16(160,122,RED,WHITE,"6");break;	
		case 7:Gui_DrawFont_GBK16(160,122,RED,WHITE,"7");break;
		case 8:Gui_DrawFont_GBK16(160,122,RED,WHITE,"8");break;	
		case 9:Gui_DrawFont_GBK16(160,122,RED,WHITE,"9");break;			
	}



}






void xiaochu()
{
	u8 n;
	//u8 p;
	//u8 c;
	for(n=20;n>0;n--)
	{
		if(n>=20)
		{
			n=20;
		}
		if(	(
					zhuangtai[n+1][1]&&zhuangtai[n+1][2]&&zhuangtai[n+1][3]&&zhuangtai[n+1][4]&&zhuangtai[n+1][5]&&zhuangtai[n+1][6]&&zhuangtai[n+1][7]&&zhuangtai[n+1][8]&&zhuangtai[n+1][9]&&zhuangtai[n+1][10]&&zhuangtai[n+1][11]&&zhuangtai[n+1][12]&&zhuangtai[n+1][13]&&zhuangtai[n+1][14]
				) 
				 &&
				!(
					zhuangtai[n][1]&&zhuangtai[n][2]&&zhuangtai[n][3]&&zhuangtai[n][4]&&zhuangtai[n][5]&&zhuangtai[n][6]&&zhuangtai[n][7]&&zhuangtai[n][8]&&zhuangtai[n][9]&&zhuangtai[n][10]&&zhuangtai[n][11]&&zhuangtai[n][12]&&zhuangtai[n][13]&&zhuangtai[n][14]
					)
		   )//描述：
		  
		{
			lie_move(10*n);//将被消除行于其上一行交换
			n=n+2;
		}
		
		if((
				!zhuangtai[n][1]&&!zhuangtai[n][2]&&!zhuangtai[n][3]&&!zhuangtai[n][4]&&!zhuangtai[n][5]&&!zhuangtai[n][6]&&!zhuangtai[n][7]&&!zhuangtai[n][8]&&!zhuangtai[n][9]&&!zhuangtai[n][10]&&!zhuangtai[n][11]&&!zhuangtai[n][12]&&!zhuangtai[n][13]&&!zhuangtai[n][14]
				)//该条件用于判断换行是否完成（因为换行瞬间两行是清空的，即均为0）
		  )
		{
			for(n=0;n<22;n++){
				if(
						(
						zhuangtai[n][1]&&zhuangtai[n][2]&&zhuangtai[n][3]&&zhuangtai[n][4]&&zhuangtai[n][5]&&zhuangtai[n][6]&&zhuangtai[n][7]&&zhuangtai[n][8]&&zhuangtai[n][9]&&zhuangtai[n][10]&&zhuangtai[n][11]&&zhuangtai[n][12]&&zhuangtai[n][13]&&zhuangtai[n][14]
						)
					){
						Del_lie(n*10);
						fengshu++;
						display_fengshu();
					if(fengshu%10==0)
					{
						speed=speed-10;
						leave++;
						display_leave();				
					}
				}	
			}
	break;		
		}
	}
}




int panduan(u16 x,u16 y,u8 what,u8 fangxiang)//碰撞检测
{
	u16 sum1=0,sum2=0;
	u8 i,n;
	u8 sbuff[23][16];
	x=x/10+1;//得到换算后的真实坐标
	y=y/10;
	for(i=0;i<23;i++)
	{
		for(n=0;n<16;n++)
		{
			sbuff[i][n]=zhuangtai[i][n];
			sum1=sum1+zhuangtai[i][n];	//若状态不改变（无碰撞），则和为60+缓存值
		}  
	}
	switch(fangxiang)
	{
		case 1:Left_zhuangtai_move((x-1)*10,10*y,what);break;
		case 2:Down_zhuangtai_move((x-1)*10,10*y,what);break;
		case 3:Right_zhuangtai_move((x-1)*10,10*y,what);break;
		case 4:change_zhuangtai();break;
		
	}
		for(i=0;i<23;i++)
	{
		
		for(n=0;n<16;n++)
		{
			sum2=sum2+zhuangtai[i][n];	
			zhuangtai[i][n]=sbuff[i][n];			
    }
	}	
		return !(sum1==sum2);
}






void game_over()
{
	u8 x;
	game=3;
	
	for(x=0;x<=86;x++)
	{
   	Gui_DrawLine(x+86,60,x+86,165,WHITE);
		Gui_DrawLine(86-x,60,86-x,165,WHITE);
	}
	
	Gui_DrawFont_GBK24(40,65,BLUE,WHITE,"游戏结束");
	Gui_DrawFont_GBK24(24,100,BLUE,WHITE,"得分：");
	Gui_DrawFont_Num32(90,95,GREEN,WHITE,fengshu/10);
	Gui_DrawFont_Num32(114,95,GREEN,WHITE,fengshu%10);
	Gui_DrawFont_GBK24(4,135,BLUE,WHITE,"请按”上“继续");
}

void first()
{ 
	u8 i,n;
	fengshu=0;
	leave=1;
	speed=80;
	Lcd_Clear(WHITE);//WHITE清除欢迎界面
  Gui_DrawLine(140,0,140,220,BLACK);//BLACK
	//清空状态
	for(i=0;i<22;i++)
	{
		for(n=1;n<15;n++)
		{
			zhuangtai[i][n]=0;
		}
	}
	//基本UI绘制
	Gui_DrawFont_GBK16(143,5,BLUE,WHITE,"NEXT");
	
	Gui_DrawFont_GBK16(143,100,BLUE,WHITE,"得分");
	Gui_DrawFont_GBK16(160,122,GREEN,WHITE,"0");
  Gui_DrawFont_GBK16(150,122,GREEN,WHITE,"0");
	
	Gui_DrawFont_GBK16(143,160,BLUE,WHITE,"Lv.");
	Gui_DrawFont_GBK16(155,182,RED,WHITE,"1");

}



void begin()//绘制方块，开始游戏
{
	Del_tuxing1(150,25,suijishu[4]);
	Del_tuxing1(150,50,suijishu[3]);
	Del_tuxing1(150,75,suijishu[2]);
	
	what=suijishu[4];//
	suijishu[4]=suijishu[3];
	suijishu[3]=suijishu[2];
	suijishu[2]=suijishu[1];
	suijishu[1]=suijishu[0];
	srand(Get_ADC_Value(3,1)*115);
	suijishu[0]=rand()%19+1;
	
	Draw_tuxing1(150,25,suijishu[4]);
	Draw_tuxing1(150,50,suijishu[3]);
	Draw_tuxing1(150,75,suijishu[2]);
	
	x=60,y=0;
	
	Draw(60,0,what);
	if(panduan(x,y,what,2))
	{
		game_over();
	}
	
}


void welcome()//欢迎界面
{
	
	
	Lcd_Clear(WHITE);
	Gui_DrawFont_GBK24(40,70,BLACK,WHITE,"欢迎来到");//-TETRIS-
	Gui_DrawFont_GBK24(50,100,YELLOW,WHITE,"-");
	Gui_DrawFont_GBK24(60,100,RED,WHITE,"T");
	Gui_DrawFont_GBK24(70,100,GREEN,WHITE,"E");
	Gui_DrawFont_GBK24(80,100,ORANGE,WHITE,"T");
	Gui_DrawFont_GBK24(90,100,PURPLE,WHITE,"R");
	Gui_DrawFont_GBK24(100,100,BLUE2,WHITE,"I");
	Gui_DrawFont_GBK24(110,100,TUHUANG,WHITE,"S");
	Gui_DrawFont_GBK24(120,100,YELLOW,WHITE,"-");
	
	Gui_DrawFont_GBK24(20,130,BLACK,WHITE,"PRESS'UP'TO STATR");
	Gui_DrawFont_GBK16(110,180,BLUE,WHITE,"@DAYceng");
}

void stop()//暂停界面
{
	u8 x;
	for(x=0;x<=86;x++)
	{
   	Gui_DrawLine(x+86,60,x+86,165,WHITE);
		Gui_DrawLine(86-x,60,86-x,165,WHITE);
	}
	
	Gui_DrawFont_GBK24(45,90,RED,WHITE,"P-A-U-S-E");
	Gui_DrawFont_GBK24(4,120,RED,WHITE,"请按”上“继续");
	
	TIM4->CR1 &= ~(0x01);//清空TIM4使能位
}	

void star()//恢复游戏运行
{
	u8 x,y;
	for(x=0;x<=86;x++)
	{
   	Gui_DrawLine(x+86,60,x+86,165,WHITE);
		Gui_DrawLine(86-x,60,86-x,165,WHITE);
	}
	
	Gui_DrawLine(140,0,140,220,BLACK);
	
	Gui_DrawFont_GBK16(143,5,BLUE,WHITE,"NEXT");
	
	Draw_tuxing1(150,25,suijishu[4]);
	Draw_tuxing1(150,50,suijishu[3]);
	Draw_tuxing1(150,75,suijishu[2]);
	
	Gui_DrawFont_GBK16(143,100,BLUE,WHITE,"得分");
	display_fengshu();
	
	Gui_DrawFont_GBK16(143,160,BLUE,WHITE,"Lv.");
	display_leave();
	
	
	for(x=1;x<15;x++)
	{
		for(y=5;y<20;y++)
		{
		if(zhuangtai[y][x])
			Draw_realbox((x-1)*10,y*10);
		}
	}
	TIM4_Init(20,36000-1);//定时1s

}
	
int main(void)
{
	//delay_ms(10);
  SystemInit();	
  SysTick_Init(72);
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);  //中断优先级分组 分2组	
	
	KEY_Init();
	Lcd_Init();
	LCD_LED_SET;
	ADCx_Init();
	TIM4_Init(20,36000-1);
	Lcd_Clear(WHITE);
	welcome();
	
	delay_ms(10);
	srand(Get_ADC_Value(3,1)*115);
	suijishu[0]=rand()%19+1;
	suijishu[1]=rand()%19+1;
	suijishu[2]=rand()%19+1;
	suijishu[3]=rand()%19+1;
	suijishu[4]=rand()%19+1;
	
	/*case对照
		#define KEY_UP 1
		#define KEY_DOWN 2
		#define KEY_LEFT 3
		#define KEY_RIGHT 4  
		#define KEY_MID 5  */
	
	while(1)
	{
		switch(KEY_Scan())
		{
			case 0:
				break;
			//功能区
			case 1:
				if(game==1){
					game=2;
					first();
					begin();
				}
				else if(game==2){
					game=4;
					stop();
				}
				else if(game==4){
					game=2;
					star();
					}
				else if(game==3){
					game=1;
				}
				break;
			
			case 2:
				if(game!=2)
					break;
				if(!panduan(x,y,what,2)){//方向判断
					Down(x,y,what);
					y=y+10;
					}else{	
						xiaochu();
						begin();
						} break;
			case 3:
				if(game!=2)
					break;
				if(!panduan(x,y,what,1)){
					Left(x,y,what);x=x-10;
					}break;
			case 4:
				if(game!=2)
					break;
				if(!panduan(x,y,what,3)){
					Right(x,y,what);x=x+10;
					}break;
			case 5:
				if(game!=2)
					break;
				if(!panduan(x,y,what,4))
					change();
					break;
	  	
		}
//方块下落		
		if(game==2)
		{
		if(i>speed)
		{
			i=0;
		
		if(panduan(x,y,what,2))//若判断碰撞且满足消除条件，返回值并调用消除函数
			{
				xiaochu();
				begin();							
		  }
			else
			{
				Down(x,y,what);		//否则匀速下移	
				y=y+10;		
			}
		}
			
}
	
	}

}

void TIM4_IRQHandler(void)//10μs中断一次
{
	if(TIM_GetITStatus(TIM4,TIM_IT_Update))
	{
		TIM_ClearITPendingBit(TIM4,TIM_IT_Update);
		i++;
	}
	
}


