#ifndef __BOX_H
#define __BOX_H 

#include "stm32f10x.h"


void Draw_realbox(u16 x,u16 y);
void Del_realbox(u16 x,u16 y);	
void Draw_tuxing(u16 x,u16 y,u8 what);
void Del_tuxing(u16 x,u16 y,u8 what);


void Draw_realbox1(u16 x,u16 y);
void Del_realbox1(u16 x,u16 y);	
void Draw_tuxing1(u16 x,u16 y,u8 what);
void Del_tuxing1(u16 x,u16 y,u8 what);


void Right_tuxing_move(u16 x,u16 y,u8 what);
	
void Down_tuxing_move(u16 x,u16 y,u8 what);
	
void Left_tuxing_move(u16 x,u16 y,u8 what);


#endif

