#include "Lcd_Driver.h"
#include "box.h"
extern u8 zhuangtai[15][22];

/*************************************************
函数名 Draw_realbox
入口参数：xy坐标
功能：画一个俄罗斯方块；
返回值：null
*************************************************/
void Draw_realbox(u16 x,u16 y)
{
	u8 i,n;
	for(i=1;i<=8;i++)//绘制小方块内填充色8*8
	{
		for(n=1;n<=8;n++)
		{
			Gui_DrawPoint((x+i),(y+n),GRAY0);	
		}	
	}
	for(i=0;i<=9;i++)//绘制小方块轮廓10*10
	{
		Gui_DrawPoint((x+i),y,BLACK	);
		Gui_DrawPoint((x+i),(y+9),BLACK	);		
		Gui_DrawPoint(x,(y+i),BLACK	);		
		Gui_DrawPoint((x+9),(y+i),BLACK	);				
	}	
}

void Draw_realbox1(u16 x,u16 y)
{
	u8 i,n;
	for(i=1;i<=3;i++)
	{
		for(n=1;n<=3;n++)
		{
			Gui_DrawPoint((x+i),(y+n),GRAY0);	
		}	
	}
	for(i=0;i<=4;i++)
	{
		Gui_DrawPoint((x+i),y,BLACK	);
		Gui_DrawPoint((x+i),(y+4),BLACK	);		
		Gui_DrawPoint(x,(y+i),BLACK	);		
		Gui_DrawPoint((x+4),(y+i),BLACK	);				
	}	
}


/*************************************************
函数名 Del_realbox
入口参数：xy坐标
功能：删除一个俄罗斯方块；
返回值：null
*************************************************/
void Del_realbox(u16 x,u16 y)
{
	u8 i,n;
	for(i=0;i<=9;i++)
	{
		for(n=0;n<=9;n++)
		{
			Gui_DrawPoint((x+i),(y+n),WHITE);	
		}	
	}
}


void Del_realbox1(u16 x,u16 y)
{
	u8 i,n;
	for(i=0;i<=4;i++)
	{
		for(n=0;n<=4;n++)
		{
			Gui_DrawPoint((x+i),(y+n),WHITE);	
		}	
	}
}



/*************************************************
函数名 Draw_tuxing
入口参数：xy坐标
功能：绘制19种完整的俄罗斯方块
返回值：null
*************************************************/
void Draw_tuxing(u16 x,u16 y,u8 what)
{
	switch (what)
	{
		case 1:
		{
		Draw_realbox(x,y);
		Draw_realbox(x+10,y);
		Draw_realbox(x,y+10);
		Draw_realbox(x+10,y+10);
		}
		break;
		
		case 2:
		{
		Draw_realbox(x,y);
		Draw_realbox(x+10,y);
		Draw_realbox(x+20,y);
		Draw_realbox(x+30,y);
		}
		break;
		
		case 3:
		{
		Draw_realbox(x,y);
		Draw_realbox(x,y+10);
		Draw_realbox(x,y+20);
		Draw_realbox(x,y+30);
		}
		break;
		
		case 4:
		{
		Draw_realbox(x+10,y);
		Draw_realbox(x,y+10);
		Draw_realbox(x+10,y+10);
		Draw_realbox(x+20,y+10);
		}
		break;
		
		case 5:
		{
		Draw_realbox(x+10,y+10);
		Draw_realbox(x,y);
		Draw_realbox(x,y+10);
		Draw_realbox(x,y+20);
		}
		break;

		case 6:
		{
		Draw_realbox(x,y+10);
		Draw_realbox(x+10,y);
		Draw_realbox(x+10,y+10);
		Draw_realbox(x+10,y+20);
		}
		break;
		
		case 7:
		{
		Draw_realbox(x+10,y+10);
		Draw_realbox(x,y);
		Draw_realbox(x+10,y);
		Draw_realbox(x+20,y);
		}
		break;
	
		case 8:
		{
		Draw_realbox(x,y);
		Draw_realbox(x,y+10);
		Draw_realbox(x,y+20);
		Draw_realbox(x+10,y+20);
		}
		break;
		
		case 9:
		{
		Draw_realbox(x,y);
		Draw_realbox(x,y+10);
		Draw_realbox(x+10,y);
		Draw_realbox(x+20,y);
		}
		break;
		
		case 10:
		{
		Draw_realbox(x,y);
		Draw_realbox(x+10,y);
		Draw_realbox(x+10,y+10);
		Draw_realbox(x+10,y+20);
		}
		break;
		
		case 11:
		{
		Draw_realbox(x,y+10);
		Draw_realbox(x+10,y+10);
		Draw_realbox(x+20,y+10);
		Draw_realbox(x+20,y);
		}
		break;
		
		case 12:
		{
		Draw_realbox(x+10,y);
		Draw_realbox(x+10,y+10);
		Draw_realbox(x+10,y+20);
		Draw_realbox(x,y+20);
		}
		break;
		
		case 13:
		{
		Draw_realbox(x,y);
		Draw_realbox(x+10,y);
		Draw_realbox(x+20,y);
		Draw_realbox(x+20,y+10);
		}
		break;
		
		case 14:
		{
		Draw_realbox(x,y);
		Draw_realbox(x+10,y);
		Draw_realbox(x,y+10);
		Draw_realbox(x,y+20);
		}
		break;
		
		case 15:
		{
		Draw_realbox(x,y);
		Draw_realbox(x,y+10);
		Draw_realbox(x+10,y+10);
		Draw_realbox(x+20,y+10);
		}
		break;
		
		
		case 16:
		{
		Draw_realbox(x+10,y);
		Draw_realbox(x,y+10);
		Draw_realbox(x+10,y+10);
		Draw_realbox(x,y+20);
		}
		break;
		
		case 17:
		{
		Draw_realbox(x,y);
		Draw_realbox(x+10,y);
		Draw_realbox(x+10,y+10);
		Draw_realbox(x+20,y+10);
		}
		break;
		
		case 18:
		{
		Draw_realbox(x,y);
		Draw_realbox(x,y+10);
		Draw_realbox(x+10,y+10);
		Draw_realbox(x+10,y+20);
		}
		break;
		
		case 19:
		{
		Draw_realbox(x,y+10);
		Draw_realbox(x+10,y+10);
		Draw_realbox(x+10,y);
		Draw_realbox(x+20,y);
		}
		break;
		
		/*case 20:
		{
		Draw_realbox(x,y);
		Draw_realbox(x+10,y);
		Draw_realbox(x+10,y);
		Draw_realbox(x+10,y);
		Draw_realbox(x,y+10);
		Draw_realbox(x+10,y+10);
		}
		break;*/
	
	
	
	
	}

}




void Draw_tuxing1(u16 x,u16 y,u8 what)
{
	switch (what)
	{
		case 1:
		{
		Draw_realbox1(x,y);
		Draw_realbox1(x+5,y);
		Draw_realbox1(x,y+5);
		Draw_realbox1(x+5,y+5);
		}
		break;
		
		case 2:
		{
		Draw_realbox1(x,y);
		Draw_realbox1(x+5,y);
		Draw_realbox1(x+10,y);
		Draw_realbox1(x+15,y);
		}
		break;
		
		case 3:
		{
		Draw_realbox1(x,y);
		Draw_realbox1(x,y+5);
		Draw_realbox1(x,y+10);
		Draw_realbox1(x,y+15);
		}
		break;
		
		case 4:
		{
		Draw_realbox1(x+5,y);
		Draw_realbox1(x,y+5);
		Draw_realbox1(x+5,y+5);
		Draw_realbox1(x+10,y+5);
		}
		break;
		
		case 5:
		{
		Draw_realbox1(x+5,y+5);
		Draw_realbox1(x,y);
		Draw_realbox1(x,y+5);
		Draw_realbox1(x,y+10);
		}
		break;

		case 6:
		{
		Draw_realbox1(x,y+5);
		Draw_realbox1(x+5,y);
		Draw_realbox1(x+5,y+5);
		Draw_realbox1(x+5,y+10);
		}
		break;
		
		case 7:
		{
		Draw_realbox1(x+5,y+5);
		Draw_realbox1(x,y);
		Draw_realbox1(x+5,y);
		Draw_realbox1(x+10,y);
		}
		break;
	
		case 8:
		{
		Draw_realbox1(x,y);
		Draw_realbox1(x,y+5);
		Draw_realbox1(x,y+10);
		Draw_realbox1(x+5,y+10);
		}
		break;
		
		case 9:
		{
		Draw_realbox1(x,y);
		Draw_realbox1(x,y+5);
		Draw_realbox1(x+5,y);
		Draw_realbox1(x+10,y);
		}
		break;
		
		case 10:
		{
		Draw_realbox1(x,y);
		Draw_realbox1(x+5,y);
		Draw_realbox1(x+5,y+5);
		Draw_realbox1(x+5,y+10);
		}
		break;
		
		case 11:
		{
		Draw_realbox1(x,y+5);
		Draw_realbox1(x+5,y+5);
		Draw_realbox1(x+10,y+5);
		Draw_realbox1(x+10,y);
		}
		break;
		
		case 12:
		{
		Draw_realbox1(x+5,y);
		Draw_realbox1(x+5,y+5);
		Draw_realbox1(x+5,y+10);
		Draw_realbox1(x,y+10);
		}
		break;
		
		case 13:
		{
		Draw_realbox1(x,y);
		Draw_realbox1(x+5,y);
		Draw_realbox1(x+10,y);
		Draw_realbox1(x+10,y+5);
		}
		break;
		
		case 14:
		{
		Draw_realbox1(x,y);
		Draw_realbox1(x+5,y);
		Draw_realbox1(x,y+5);
		Draw_realbox1(x,y+10);
		}
		break;
		
		case 15:
		{
		Draw_realbox1(x,y);
		Draw_realbox1(x,y+5);
		Draw_realbox1(x+5,y+5);
		Draw_realbox1(x+10,y+5);
		}
		break;
		
		
		case 16:
		{
		Draw_realbox1(x+5,y);
		Draw_realbox1(x,y+5);
		Draw_realbox1(x+5,y+5);
		Draw_realbox1(x,y+10);
		}
		break;
		
		case 17:
		{
		Draw_realbox1(x,y);
		Draw_realbox1(x+5,y);
		Draw_realbox1(x+5,y+5);
		Draw_realbox1(x+10,y+5);
		}
		break;
		
		case 18:
		{
		Draw_realbox1(x,y);
		Draw_realbox1(x,y+5);
		Draw_realbox1(x+5,y+5);
		Draw_realbox1(x+5,y+10);
		}
		break;
		
		case 19:
		{
		Draw_realbox1(x,y+5);
		Draw_realbox1(x+5,y+5);
		Draw_realbox1(x+5,y);
		Draw_realbox1(x+10,y);
		}
		break;
	
	
	
	
	}

}



/*************************************************
函数名 Del_tuxing
入口参数：xy坐标
功能：
返回值：null
*************************************************/
void Del_tuxing(u16 x,u16 y,u8 what)
{
	switch (what)
	{
		case 1:
		{
		Del_realbox(x,y);
		Del_realbox(x+10,y);
		Del_realbox(x,y+10);
		Del_realbox(x+10,y+10);
		}
		break;
		
		case 2:
		{
		Del_realbox(x,y);
		Del_realbox(x+10,y);
		Del_realbox(x+20,y);
		Del_realbox(x+30,y);
		}
		break;
		
		case 3:
		{
		Del_realbox(x,y);
		Del_realbox(x,y+10);
		Del_realbox(x,y+20);
		Del_realbox(x,y+30);
		}
		break;
		
		case 4:
		{
		Del_realbox(x+10,y);
		Del_realbox(x,y+10);
		Del_realbox(x+10,y+10);
		Del_realbox(x+20,y+10);
		}
		break;
		
		case 5:
		{
		Del_realbox(x+10,y+10);
		Del_realbox(x,y);
		Del_realbox(x,y+10);
		Del_realbox(x,y+20);
		}
		break;

		case 6:
		{
		Del_realbox(x,y+10);
		Del_realbox(x+10,y);
		Del_realbox(x+10,y+10);
		Del_realbox(x+10,y+20);
		}
		break;
		
		case 7:
		{
		Del_realbox(x+10,y+10);
		Del_realbox(x,y);
		Del_realbox(x+10,y);
		Del_realbox(x+20,y);
		}
		break;
	
		case 8:
		{
		Del_realbox(x,y);
		Del_realbox(x,y+10);
		Del_realbox(x,y+20);
		Del_realbox(x+10,y+20);
		}
		break;
		
		case 9:
		{
		Del_realbox(x,y);
		Del_realbox(x,y+10);
		Del_realbox(x+10,y);
		Del_realbox(x+20,y);
		}
		break;
		
		case 10:
		{
		Del_realbox(x,y);
		Del_realbox(x+10,y);
		Del_realbox(x+10,y+10);
		Del_realbox(x+10,y+20);
		}
		break;
		
		case 11:
		{
		Del_realbox(x,y+10);
		Del_realbox(x+10,y+10);
		Del_realbox(x+20,y+10);
		Del_realbox(x+20,y);
		}
		break;
		
		case 12:
		{
		Del_realbox(x+10,y);
		Del_realbox(x+10,y+10);
		Del_realbox(x+10,y+20);
		Del_realbox(x,y+20);
		}
		break;
		
		case 13:
		{
		Del_realbox(x,y);
		Del_realbox(x+10,y);
		Del_realbox(x+20,y);
		Del_realbox(x+20,y+10);
		}
		break;
		
		case 14:
		{
		Del_realbox(x,y);
		Del_realbox(x+10,y);
		Del_realbox(x,y+10);
		Del_realbox(x,y+20);
		}
		break;
		
		case 15:
		{
		Del_realbox(x,y);
		Del_realbox(x,y+10);
		Del_realbox(x+10,y+10);
		Del_realbox(x+20,y+10);
		}
		break;
		
		
		case 16:
		{
		Del_realbox(x+10,y);
		Del_realbox(x,y+10);
		Del_realbox(x+10,y+10);
		Del_realbox(x,y+20);
		}
		break;
		
		case 17:
		{
		Del_realbox(x,y);
		Del_realbox(x+10,y);
		Del_realbox(x+10,y+10);
		Del_realbox(x+20,y+10);
		}
		break;
		
		case 18:
		{
		Del_realbox(x,y);
		Del_realbox(x,y+10);
		Del_realbox(x+10,y+10);
		Del_realbox(x+10,y+20);
		}
		break;
		
		case 19:
		{
		Del_realbox(x,y+10);
		Del_realbox(x+10,y+10);
		Del_realbox(x+10,y);
		Del_realbox(x+20,y);
		}
		break;
	
	
	
	
	}

}


void Del_tuxing1(u16 x,u16 y,u8 what)
{
	switch (what)
	{
		case 1:
		{
		Del_realbox1(x,y);
		Del_realbox1(x+5,y);
		Del_realbox1(x,y+5);
		Del_realbox1(x+5,y+5);
		}
		break;
		
		case 2:
		{
		Del_realbox1(x,y);
		Del_realbox1(x+5,y);
		Del_realbox1(x+10,y);
		Del_realbox1(x+15,y);
		}
		break;
		
		case 3:
		{
		Del_realbox1(x,y);
		Del_realbox1(x,y+5);
		Del_realbox1(x,y+10);
		Del_realbox1(x,y+15);
		}
		break;
		
		case 4:
		{
		Del_realbox1(x+5,y);
		Del_realbox1(x,y+5);
		Del_realbox1(x+5,y+5);
		Del_realbox1(x+10,y+5);
		}
		break;
		
		case 5:
		{
		Del_realbox1(x+5,y+5);
		Del_realbox1(x,y);
		Del_realbox1(x,y+5);
		Del_realbox1(x,y+10);
		}
		break;

		case 6:
		{
		Del_realbox1(x,y+5);
		Del_realbox1(x+5,y);
		Del_realbox1(x+5,y+5);
		Del_realbox1(x+5,y+10);
		}
		break;
		
		case 7:
		{
		Del_realbox1(x+5,y+5);
		Del_realbox1(x,y);
		Del_realbox1(x+5,y);
		Del_realbox1(x+10,y);
		}
		break;
	
		case 8:
		{
		Del_realbox1(x,y);
		Del_realbox1(x,y+5);
		Del_realbox1(x,y+10);
		Del_realbox1(x+5,y+10);
		}
		break;
		
		case 9:
		{
		Del_realbox1(x,y);
		Del_realbox1(x,y+5);
		Del_realbox1(x+5,y);
		Del_realbox1(x+10,y);
		}
		break;
		
		case 10:
		{
		Del_realbox1(x,y);
		Del_realbox1(x+5,y);
		Del_realbox1(x+5,y+5);
		Del_realbox1(x+5,y+10);
		}
		break;
		
		case 11:
		{
		Del_realbox1(x,y+5);
		Del_realbox1(x+5,y+5);
		Del_realbox1(x+10,y+5);
		Del_realbox1(x+10,y);
		}
		break;
		
		case 12:
		{
		Del_realbox1(x+5,y);
		Del_realbox1(x+5,y+5);
		Del_realbox1(x+5,y+10);
		Del_realbox1(x,y+10);
		}
		break;
		
		case 13:
		{
		Del_realbox1(x,y);
		Del_realbox1(x+5,y);
		Del_realbox1(x+10,y);
		Del_realbox1(x+10,y+5);
		}
		break;
		
		case 14:
		{
		Del_realbox1(x,y);
		Del_realbox1(x+5,y);
		Del_realbox1(x,y+5);
		Del_realbox1(x,y+10);
		}
		break;
		
		case 15:
		{
		Del_realbox1(x,y);
		Del_realbox1(x,y+5);
		Del_realbox1(x+5,y+5);
		Del_realbox1(x+10,y+5);
		}
		break;
		
		
		case 16:
		{
		Del_realbox1(x+5,y);
		Del_realbox1(x,y+5);
		Del_realbox1(x+5,y+5);
		Del_realbox1(x,y+10);
		}
		break;
		
		case 17:
		{
		Del_realbox1(x,y);
		Del_realbox1(x+5,y);
		Del_realbox1(x+5,y+5);
		Del_realbox1(x+10,y+5);
		}
		break;
		
		case 18:
		{
		Del_realbox1(x,y);
		Del_realbox1(x,y+5);
		Del_realbox1(x+5,y+5);
		Del_realbox1(x+5,y+10);
		}
		break;
		
		case 19:
		{
		Del_realbox1(x,y+5);
		Del_realbox1(x+5,y+5);
		Del_realbox1(x+5,y);
		Del_realbox1(x+10,y);
		}
		break;
	
	
	
	
	}

}





/*************************************************
函数名 Down_mov
入口参数：xy坐标
功能：
返回值：null
*************************************************/


void Down_tuxing_move(u16 x,u16 y,u8 what)
{
	Del_tuxing(x,y,what);
	Draw_tuxing(x,y+10,what);
}

/*************************************************
函数名 Left_mov
入口参数：xy坐标
功能：
返回值：null
*************************************************/


void Left_tuxing_move(u16 x,u16 y,u8 what)
{
	Del_tuxing(x,y,what);
	Draw_tuxing(x-10,y,what);
}

/*************************************************
函数名 Right_mov
入口参数：xy坐标
功能：
返回值：null
*************************************************/

void Right_tuxing_move(u16 x,u16 y,u8 what)
{
	Del_tuxing(x,y,what);
	Draw_tuxing(x+10,y,what);
}



  








