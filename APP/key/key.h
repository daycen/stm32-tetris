#ifndef _key_H
#define _key_H


#include "system.h"
 
 
 
#define KEY_UP_Pin      GPIO_Pin_0   
#define KEY_DOWN_Pin    GPIO_Pin_1 
#define KEY_LEFT_Pin    GPIO_Pin_2  
#define KEY_RIGHT_Pin   GPIO_Pin_6  
#define KEY_MID_Pin      GPIO_Pin_8

#define KEY_Port (GPIOA) //定义端口



//使用位操作定义
#define K_UP PAin(0)
#define K_DOWN PAin(1)
#define K_LEFT PAin(2)
#define K_RIGHT PAin(6)
#define K_MID PAin(8)

//使用读取管脚状态库函数定义 
//#define K_UP      GPIO_ReadInputDataBit(KEY_UP_Port,KEY_UP_Pin)
//#define K_DOWN    GPIO_ReadInputDataBit(KEY_Port,KEY_DOWN_Pin)
//#define K_LEFT    GPIO_ReadInputDataBit(KEY_Port,KEY_LEFT_Pin)
//#define K_RIGHT   GPIO_ReadInputDataBit(KEY_Port,KEY_RIGHT_Pin)


//定义各个按键值  
#define KEY_UP 1
#define KEY_DOWN 2
#define KEY_LEFT 3
#define KEY_RIGHT 4  
#define KEY_MID 5  



void KEY_Init(void);
u8 KEY_Scan(void);
#endif
